package com.wx.main;


import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * 获取Access_token
 */
public class Wx_Access_Token {

    public static String getAccessToken(String appId,String appSecret) throws Exception {
        //定义请求地址(只需要填上appid和secret就OK)
        String tokenUrl="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appId+"&secret="+appSecret+"";
        //建立连接
        URL url = new URL(tokenUrl);
        HttpsURLConnection httpUrlConn = (HttpsURLConnection)url.openConnection();

        //创建SSLContext对象，使用指定的信任管理器初始化
        TrustManager[] tm = {new MyX509TrustManager()};
        SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
        sslContext.init(null,tm,new SecureRandom());

        //从上传输SSLContext对象中获取SSLSocketFactory对象
        SSLSocketFactory ssf = sslContext.getSocketFactory();

        httpUrlConn.setSSLSocketFactory(ssf);
        httpUrlConn.setDoInput(true);
        httpUrlConn.setDoOutput(true);

        //设置请求方式
        httpUrlConn.setRequestMethod("GET");

        //获取输入流
        InputStream inputStream = httpUrlConn.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        //读取响应内容
        StringBuffer buffer = new StringBuffer();
        String str = null;
        while ((str = bufferedReader.readLine())!=null){
            buffer.append(str);
        }
        //释放资源
        bufferedReader.close();
        inputStreamReader.close();
        inputStream.close();
        httpUrlConn.disconnect();

        return buffer.toString();
    }
    static class MyX509TrustManager implements X509TrustManager {
        //检查客户端证书
        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

        }

        //检查服务器端证书
        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

        }

        //返回收信人的x509证书数组
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }
}
